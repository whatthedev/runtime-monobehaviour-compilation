EXAMPLE MONOBEHAVIOUR USED ON STREAM:

    using System;
    using UnityEngine;
    namespace TranslateExample
    {

        public class CompiledTranslateBehaviour : MonoBehaviour
        {
	    private bool _hasNotPrinted = true;
	    public Transform _factoryTransform;
	    public string PrintText;
            void Update()
            {
		if(PrintText != null && _hasNotPrinted)
		{
		    _hasNotPrinted = false;
		    print(PrintText);
		}

                transform.Translate(transform.forward * 5 * Time.deltaTime);
            }
        }
    }


NAMESPACE REQUIRED TO ACCESS IT

TranslateExample.CompiledTranslateBehaviour