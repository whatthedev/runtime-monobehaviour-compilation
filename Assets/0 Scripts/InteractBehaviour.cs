﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractBehaviour : MonoBehaviour
{
    [SerializeField] private RoslynBehaviour _roslyn;

    void OnCollisionEnter(Collision coll)
    {
        _roslyn.ToggleCanvas();
    }
}
