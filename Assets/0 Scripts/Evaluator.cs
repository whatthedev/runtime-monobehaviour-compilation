﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.Emit;
using UnityEngine;

namespace Novaluator
{
    public class Evaluator
    {

        //private static readonly ImmutableArray<Assembly> _defaultReferences =
        //    ImmutableArray.Create(
        //        typeof(Enumerable).GetTypeInfo().Assembly,
        //        typeof(List<string>).GetTypeInfo().Assembly,
        //        typeof(string).GetTypeInfo().Assembly,

        //    );


        public static Type CompileMonoBehaviour(string code, string typeUri)
        {
            SyntaxTree syntaxTree = CSharpSyntaxTree.ParseText(code);
            string assemblyName = Path.GetRandomFileName();
            MetadataReference[] references = new MetadataReference[]
            {
                MetadataReference.CreateFromFile(typeof(object).Assembly.Location),
                MetadataReference.CreateFromFile(typeof(Enumerable).Assembly.Location),
                MetadataReference.CreateFromFile(typeof(MonoBehaviour).Assembly.Location)
            };

            CSharpCompilation compilation = CSharpCompilation.Create(
                assemblyName,
                new[] { syntaxTree },
                references,
                new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary));
            using (var ms = new MemoryStream())
            {
                EmitResult result = compilation.Emit(ms);

                if (!result.Success)
                {
                    IEnumerable<Diagnostic> failures = result.Diagnostics.Where(diagnostic =>
                        diagnostic.IsWarningAsError ||
                        diagnostic.Severity == DiagnosticSeverity.Error);
                    StringBuilder sb = new StringBuilder();
                    foreach (Diagnostic diagnostic in failures)
                    {
                        sb.AppendLine(($"{diagnostic.Id}: {diagnostic.GetMessage()}"));
                    }
                    throw new Exception("Compilation failed! Reasons: " + sb);
                }
                else
                {
                    ms.Seek(0, SeekOrigin.Begin);
                    Assembly assembly = Assembly.Load(ms.ToArray());
                    Type type = assembly.GetType(typeUri);
                    return type;
                }
            }

        }
    }
}
