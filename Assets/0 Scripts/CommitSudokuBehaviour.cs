﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(RoslynBehaviour))]
public class CommitSudokuBehaviour : MonoBehaviour
{
    private RoslynBehaviour _roslyn;
    private RoslynFactory _factory;
    [SerializeField] private float _cooldownWindow;
    private float _timePassed;
    private GameObject _targetObjectToClone;

    // Use this for initialization
    void Start()
    {
        _targetObjectToClone = GameObject.CreatePrimitive(PrimitiveType.Cube);
        _targetObjectToClone.transform.position = new Vector3(999,999,999);
        _roslyn = GetComponent<RoslynBehaviour>();
        _factory = new RoslynFactory();
    }

    // Update is called once per frame
    void Update()
    {
        if (_roslyn.ResultType != null && _timePassed >= _cooldownWindow)
        {
            _factory.Create(_roslyn, _targetObjectToClone, new object[]{transform, "did it do the thing? I think it did the thing." });
            _timePassed = 0f;
        }

        _timePassed += Time.deltaTime; //TODO: end me
    }
}
