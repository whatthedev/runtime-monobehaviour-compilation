﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UObject = UnityEngine.Object;


public class RoslynFactory
{
    public void Create(RoslynBehaviour behaviour, GameObject targetGo, object[] args = null)
    {
        GameObject go = UObject.Instantiate(targetGo, Vector3.zero, new Quaternion(0, 0, 0, 0));
        Component comp = go.AddComponent(behaviour.ResultType);
        if (args != null)
        {
            List<FieldInfo> fields = behaviour.ResultType.GetFields()
                .Where(x =>  x.IsPublic).ToList();
            for (int i = 0; i < args.Length; i++)
            {
                fields[i].SetValue(comp, args[i]);
            }
        }
    }
}