﻿using System;
using System.Collections;
using System.Collections.Generic;
using Novaluator;
using UnityEngine;
using UnityEngine.UI;


public class RoslynBehaviour : MonoBehaviour
{
    [SerializeField] private InputField _code;
    [SerializeField] private InputField _namespace;
    [SerializeField] private InputField _methodName;
    [SerializeField] private Canvas _canvas;
    [SerializeField] private float _coolDownWindow;
    public Type ResultType;

    public void InvokeInGameCode()
    {
        ResultType = Evaluator.CompileMonoBehaviour(_code.text, _namespace.text);
        ToggleCanvas();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
           ToggleCanvas();
        }
    }

    public void ToggleCanvas()
    {
        _canvas.gameObject.SetActive(!_canvas.gameObject.activeInHierarchy);
    }
}
